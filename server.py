import sys
import socket
import threading
import datetime


lista_clientes = {}
ENCODE = "utf-8"


def log_generic(msg):
    tday = datetime.datetime.now()
    logs = open("log_server.txt", "a")
    msg = f"[{tday.isoformat(' ')}] {msg}"
    print(msg)
    logs.write(msg + "\n")
    logs.close()


def parse_msg(conn, msg_ori):
    msgs = []
    try:
        msgs = msg_ori.split(" ")
    except BaseException:
        log_generic("Mensagem inválida recebido")
        msg = "nmsg fail"
        conn.send(msg.encode(encoding=ENCODE))
        return False, []

    if len(msgs) < 1:
        log_generic("Mensagem inválida recebido")
        msg = "nmsg fail"
        conn.send(msg.encode(encoding=ENCODE))
        return False, []

    if len(msgs) < 2:
        msg = f"{msgs[0]} nok"
        log_generic(msg)
        conn.send(msg.encode(encoding=ENCODE))
        return False, []
    log_generic(msgs)
    return True, msgs


def main():
    host, port = sys.argv[1], int(sys.argv[2])
    lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    lsock.bind((host, port))
    lsock.listen()
    log_generic(f"Servidor iniciado, ouvindo {host}:{port}")

    try:
        while True:
            aceita_conexao(lsock)
    except KeyboardInterrupt:
        log_generic(f"Fechando servidor")


def aceita_conexao(sock):
    conn, addr = sock.accept()
    log_generic(f"Aceitou conexao de {addr}")
    thread = threading.Thread(target=handle_requisicao_cliente, args=[conn, addr])
    thread.start()


def handle_requisicao_cliente(conn, addr):
    while True:
        try:
            user_input = conn.recv(1024).decode(encoding=ENCODE)
            ok, msgs = parse_msg(conn, user_input)
            if ok and msgs[0] == "registro" and len(msgs) >= 4:
                handle_registra_usuario(conn, msgs[1], msgs[2], msgs[3]) 
            elif ok and msgs[0] == "consulta" and len(msgs) >= 2:
                handle_consulta_usuario(conn, msgs[1])
            elif ok and msgs[0] == "fechamento" and len(msgs) >= 2:
                handle_fecha_conexao(conn, msgs[1])
                conn.close()
                break

        except Exception:
            log_generic(f"{addr[0]}:{addr[1]} se desconectou")
            # lista_clientes.pop(username)
            conn.close()
            break


def existe_username(username):
    return username in lista_clientes.keys()


def get_cliente(username):
    return lista_clientes[username]


def handle_registra_usuario(conn, username, ip, porta):
    if existe_username(username):
        msg = "registro existe"
        log_generic(msg)
        conn.send(msg.encode(encoding=ENCODE))
    else:
        lista_clientes[username] = {'ip': ip, 'porta': porta}
        msg = "registro ok"
        log_generic(msg)
        conn.send(msg.encode(encoding=ENCODE))


def handle_consulta_usuario(conn, username):
    if existe_username(username):
        cliente = get_cliente(username)
        msg = f"consulta {username} {cliente['ip']} {cliente['porta']}"
        conn.send(msg.encode(encoding=ENCODE))
    else:
        msg = "consulta nexiste"
        conn.send(msg.encode(encoding=ENCODE))


def handle_fecha_conexao(conn, username):
    log_generic(f"fechamento {username}")
    lista_clientes.pop(username)


if __name__ == "__main__":
    log_generic("======== Server iniciando ========")
    main()
