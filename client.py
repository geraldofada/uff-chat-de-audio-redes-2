import base64
import socket
import datetime
import logging
import sys
import threading

import pyaudio

ENCODE = "utf-8"
HOST = "127.0.0.1"  # The server's hostname or IP address
PORT = 5000  # The port used by the server
HOST_LIGACAO = ""

CHUNK = 512
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100

lista_usuarios_consultados = {}


def conecta_server_registro():
    lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    lsock.connect((HOST, PORT))

    print("Qual seu nome de usuário?")
    username = input("> ")

    # pegar o ip local do cara aqui para o UDP
    msg = f"registro {username} {HOST_LIGACAO} {6000}"
    lsock.send(msg.encode(encoding=ENCODE))
    log_msg_socket(msg, "TCP", True, HOST_LIGACAO, HOST, PORT)

    recv = lsock.recv(1024).decode(encoding=ENCODE)
    log_msg_socket(recv, "TCP", False, HOST_LIGACAO, HOST, PORT)
    while recv.split(" ")[1] == "existe":
        print("Usuário já existe.")
        print("Qual seu nome de usuário?")
        username = input("> ")

        msg = f"registro {username} {HOST_LIGACAO} {6000}"
        lsock.send(msg.encode(encoding=ENCODE))
        log_msg_socket(msg, "TCP", True, HOST_LIGACAO, HOST, PORT)

        recv = lsock.recv(1024).decode(encoding=ENCODE)
        log_msg_socket(recv, "TCP", False, HOST_LIGACAO, HOST, PORT)

    print("Conexão com o server de registro estabelecida com sucesso")
    return lsock, username


def recebe_ligacao(socket_ligacao, endereco, recebeu_ligacao):
    try:
        recebeu_ligacao[0] = False
        endereco[0] = ()
        print("Esperando uma ligação")

        dados, addr = socket_ligacao.recvfrom(CHUNK*3)
        log_msg_socket(dados, "UDP", False, HOST_LIGACAO, addr[0], addr[1])

        msg, username_ligando = dados.decode(encoding=ENCODE).split(" ")
        if msg == "audio":
            dados, addr = socket_ligacao.recvfrom(CHUNK * 3)
            log_msg_socket(dados, "UDP", False, HOST_LIGACAO, addr[0], addr[1])

            msg, username_ligando = dados.decode(encoding=ENCODE).split(" ")
        if msg == "convite":
            print(f"Recebendo ligação de {username_ligando}")
            print("O que deseja fazer?")
            print("a) Aceitar")
            print("r) Recursar")
            recebeu_ligacao[0] = True
            endereco[0] = addr
    except SystemExit:
        print("CABO")


def responde_ligacao(socket_ligacao, user_in, addr):
    if user_in == "a":
        msg = f"resposta_ao_convite aceito"
        socket_ligacao.sendto(msg.encode(encoding=ENCODE), addr)
        log_msg_socket(msg, "UDP", True, HOST_LIGACAO, addr[0], addr[1])

        termina = [False]
        handle_input_microfone(socket_ligacao, addr, termina)
        encerra_chamada = input("Para encerrar a chamada digite sair\n")
        while encerra_chamada != "sair":
            encerra_chamada = input("Para encerrar a chamada digite sair\n")
        termina[0] = True

        msg = "encerrar_ligação _"
        socket_ligacao.sendto(msg.encode(encoding=ENCODE), addr)
        log_msg_socket(msg, "UDP", True, HOST_LIGACAO, addr[0], addr[1])
    elif user_in == "r":
        msg = f"resposta_ao_convite recusado"
        socket_ligacao.sendto(msg.encode(encoding=ENCODE), addr)
        log_msg_socket(msg, "UDP", True, HOST_LIGACAO, addr[0], addr[1])


def inicia_ligacao(username):
    print_lista_contatos()
    print("Qual username deseja ligar?\n(Se deseja sair do menu de ligação digite sair)\n")
    username_ligacao = input()
    if username_ligacao == "sair":
        return
    if username_ligacao in lista_usuarios_consultados.keys():
        host = lista_usuarios_consultados[username_ligacao]["ip"]  # The server's hostname or IP address
        port = int(lista_usuarios_consultados[username_ligacao]["porta"])  # The port used by the server

        lsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        msg = f"convite {username}"
        lsock.sendto(msg.encode(encoding=ENCODE), (host, port))
        log_msg_socket(msg, "UDP", True, HOST_LIGACAO, host, port)

        user_input, addr = lsock.recvfrom(1024)
        log_msg_socket(user_input, "UDP", False, HOST_LIGACAO, addr[0], addr[1])

        _, resposta = user_input.decode(encoding=ENCODE).split(" ")
        if resposta == "recusado":
            print("usuário destino ocupado")
        elif resposta == "aceito":
            termina = [False]
            handle_input_microfone(lsock, addr, termina)
            encerra_chamada = input("Para encerrar a chamada digite sair\n")
            while encerra_chamada != "sair":
                encerra_chamada = input("Para encerrar a chamada digite sair\n")
            termina[0] = True
            msg = "encerrar_ligação _"
            lsock.sendto(msg.encode(encoding=ENCODE), addr)
            log_msg_socket(msg, "UDP", True, HOST_LIGACAO, host, port)


def handle_input_microfone(socket_ligacao, addr, termina):
    p = pyaudio.PyAudio()

    stream_recebe = p.open(format=FORMAT,
                           channels=CHANNELS,
                           rate=RATE,
                           output=True,
                           frames_per_buffer=CHUNK)

    stream_envia = p.open(format=FORMAT,
                          channels=CHANNELS,
                          rate=RATE,
                          input=True,
                          frames_per_buffer=CHUNK)

    thread_recebe = threading.Thread(target=recebe_audio, args=[stream_recebe, socket_ligacao, termina])
    thread_envia = threading.Thread(target=envia_audio, args=[stream_envia, socket_ligacao, addr, termina])
    thread_recebe.start()
    thread_envia.start()


def recebe_audio(stream, socket_ligacao, termina):
    while not termina[0]:
        user_input, addr = socket_ligacao.recvfrom(CHUNK*3)
        log_msg_socket(user_input, "UDP", False, HOST_LIGACAO, addr[0], addr[1])

        tipo, data = user_input.decode(encoding=ENCODE).split(" ")
        if tipo == "encerrar_ligação":
            termina[0] = True
        else:
            stream.write(base64.b64decode(data))
            free = stream.get_write_available()
            if free > CHUNK:
                to_fill = free - CHUNK
                stream.write(chr(0)*CHANNELS*2*to_fill)


def envia_audio(stream, socket_ligacao, addr, termina):
    while not termina[0]:
        data = stream.read(CHUNK)
        str_data = base64.b64encode(data).decode("ascii")
        msg = f"audio {str_data}"
        socket_ligacao.sendto(msg.encode(encoding=ENCODE), addr)
        log_msg_socket(msg, "UDP", True, HOST_LIGACAO, addr[0], addr[1])


def consulta_usuario(lsock):
    username = input("Qual username quer consultar?\n")

    msg = f"consulta {username}"
    lsock.send(msg.encode(encoding=ENCODE))
    log_msg_socket(msg, "TCP", True, HOST_LIGACAO, HOST, PORT)

    resultado = lsock.recv(1024).decode(encoding=ENCODE)
    log_msg_socket(resultado, "TCP", False, HOST_LIGACAO, HOST, PORT)

    print(msg)
    if resultado.split(" ")[1] != "nexiste":
        _, username_input, ip, porta = resultado.split(" ")
        print("Username encontrado, adicionando a lista de usuarios consultados")
        lista_usuarios_consultados[username_input] = {'ip': ip, 'porta': porta}
        print(f"Username: {username_input}, endereço: {ip}:{porta}")
    else:
        print("Username não encontrado")


def log_generic(msg):
    tday = datetime.datetime.now()
    logs = open("log_cliente.txt", "a")
    logs.write(f"[{tday.isoformat(' ')}] {msg}\n")
    logs.close()


def log_msg_socket(msg, protocolo, enviado, de, para, porta):
    tday = datetime.datetime.now()
    logs = open("log_cliente.txt", "a")
    sent = "e" if enviado else "r"
    logs.write(f"[{tday.isoformat(' ')}] {sent} {protocolo.upper()}: {msg} \t{de} <-> {para}:{porta}\n")
    logs.close()


def mostra_log():
    with open("log_cliente.txt", "r+") as logs:
        print(logs.read())


def encerra_conexao(lsock, username):
    print("Encerrando conexão")
    msg = f"fechamento {username}"
    lsock.send(msg.encode(encoding=ENCODE))
    log_msg_socket(msg, "TCP", True, HOST_LIGACAO, HOST, PORT)


def recebe_input():
    print()
    print("1) Consultar usuário")
    print("2) Realizar ligação")
    print("3) Mostrar logs")
    print("4) Encerrar conexão")
    user_input = input("> ")
    return user_input


def inicia_server_ligacao():
    host, port = sys.argv[1], 6000
    lsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    lsock.bind((host, port))
    print(f"Ouvindo {host}:{port}")
    global HOST_LIGACAO
    HOST_LIGACAO = host
    return lsock


def print_lista_contatos():
    print("Usernames conhecidos:")
    for key in lista_usuarios_consultados.keys():
        print("- "+key)
    print()


def main():
    log_generic("========= Cliente inicializando =========")
    socket_ligacao = inicia_server_ligacao()
    lsock, username = conecta_server_registro()

    endereco = [()]
    recebeu_ligacao = [False]
    thread = threading.Thread(target=recebe_ligacao, args=[socket_ligacao, endereco, recebeu_ligacao])
    thread.start()
    while True:
        try:
            user_in = recebe_input()
            if user_in == "1":
                consulta_usuario(lsock)
            elif user_in == "2":
                inicia_ligacao(username)
            elif user_in == "3":
                mostra_log()
            elif user_in == "4":
                encerra_conexao(lsock, username)
                sys.exit()
            elif (user_in == "a" or user_in == "r") and recebeu_ligacao[0]:
                responde_ligacao(socket_ligacao, user_in, endereco[0])
                thread = threading.Thread(target=recebe_ligacao, args=[socket_ligacao, endereco, recebeu_ligacao])
                thread.start()
            else:
                print("Opção inválida")
        except SystemExit:
            return
        except BaseException as e:
            print(e)
            logging.exception("An exception was thrown!")
            encerra_conexao(lsock, username)
            sys.exit()


if __name__ == "__main__":
    main()
